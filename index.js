const animeUrl =
  "https://kitsu.io/api/edge/anime?filter[categories]=adventure&page[limit]=15";
const animeListDiv = document.getElementById("anime-list");
const searchInput = document.getElementById("search-input");

async function getAnimeList(filter = "") {
  const response = await fetch(animeUrl + filter);
  const { data } = await response.json();
  return data;
}

function getVideo(videoId) {
  return `<iframe width="320" src="https://www.youtube.com/embed/${videoId}?si=-sVxaBW3hu2RQqpc" 
  title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; 
  gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>`;
}

function fillCardInfo(anime) {
  const {
    posterImage,
    canonicalTitle,
    averageRating,
    ageRatingGuide,
    status,
    startDate,
    endDate,
  } = anime.attributes;

  return `<div class="card">
      <img
        src="${posterImage.small}"
        class="card-img-top"
        alt="..."
      />
      <div class="card-body">
        <h6 class="card-title">${canonicalTitle}</h6>
        <p class="card-text">
          <small class="text-muted">Rating: ${averageRating}</small>
          <p>Status: ${status}</p>
          <p>${startDate} - ${endDate}</p>
        </p>
      </div>
      <ul class="list-group list-group-flush">
        <li class="list-group-item">${ageRatingGuide}</li>
      </ul>
      <div class="card-body">
        <a href="#" class="card-link" data-bs-toggle="offcanvas"
        data-bs-target="#offcanvasRight${anime.id}"
        aria-controls="offcanvasRight">Detailed</a>
      </div>
    </div>`;
}

function addAnimeOffcanvas(anime) {
  const {
    canonicalTitle,
    description,
    startDate,
    endDate,
    youtubeVideoId,
    episodeCount,
    episodeLength
  } = anime.attributes;

  const newDiv = document.createElement("div");
  newDiv.setAttribute("class", "offcanvas offcanvas-end");
  newDiv.setAttribute("tabindex", "-1");
  newDiv.setAttribute("id", `offcanvasRight${anime.id}`);
  newDiv.setAttribute("aria-labelledby", "offcanvasRightLabel");
  newDiv.innerHTML = `
  <div class="offcanvas-header">
    <h5 id="offcanvasRightLabel">${canonicalTitle}</h5>
    <button
      type="button"
      class="btn-close text-reset"
      data-bs-dismiss="offcanvas"
      aria-label="Close"
    ></button>
  </div>
  <div class="offcanvas-body">
  <ul class="list-group list-group-flush">
  <li class="list-group-item">${getVideo(youtubeVideoId)}</li>
  <li class="list-group-item">${startDate} - ${endDate}</li>
  <li class="list-group-item">${episodeCount} episodes</li>
  <li class="list-group-item">Episode duration - ${episodeLength}</li>
  <li class="list-group-item">${description}</li>
  </ul></div>`;
  document.body.appendChild(newDiv);
}

function createAnimeCard(anime) {
  addAnimeOffcanvas(anime);

  const newCol = document.createElement("div");
  newCol.className = "col-3";
  newCol.innerHTML = fillCardInfo(anime);
  animeListDiv.appendChild(newCol);
}

async function load(searchString = "") {
  if (!searchString) {
    searchInput.value = "";
  }
  animeListDiv.innerHTML = `<div class="d-flex justify-content-center">
  <div class="spinner-border" role="status">
    <span class="visually-hidden">Loading...</span>
  </div>
</div>`;
  const data = await getAnimeList(searchString);
  animeListDiv.innerHTML = "";
  if (!data.length) {
    animeListDiv.innerHTML = "<div>No matches found</div>";
  }
  for (const anime of data) {
    createAnimeCard(anime);
  }
}

load();

function handleSearchInput() {
  load(`&filter[text]=${this.value}`);
}

searchInput.addEventListener("change", handleSearchInput);
searchInput.addEventListener("keyup", handleSearchInput);
